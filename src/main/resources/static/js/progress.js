progress = {
	start : function(zin) {
	  zin = zin || 1000;
	  $("body").append("<div id=\"ajaxrequestopen\" class=\"loading\" style=\"z-index: " + zin + " !important\"><i class=\"fa fa-spinner fa-pulse\"></i></div>");
	},
	stop : function() {
		$("#ajaxrequestopen").remove();
	},
	start_by_selector : function(selector) {
		var hiddenChildren = $('<div id="hidden_children" style="display: none">').html($(selector).html());
		$(selector).html("").addClass("loading_part").append("<i class=\"fa fa-spinner fa-pulse\"></i>").append(hiddenChildren);
	},
	set_in_selector : function(selector, html) {
		$(selector + " > #hidden_children").html(html);
	},
	stop_by_selector : function(selector) {
		$(selector).removeClass("loading_part");
		$(selector + " > .fa-spinner").remove();
		var hiddenChildren = $(selector + " > #hidden_children").html();
		$(selector).html(hiddenChildren); // overwrites the #hidden_children subcomponent automatically
	}
}