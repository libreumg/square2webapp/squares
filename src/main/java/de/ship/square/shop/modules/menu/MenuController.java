package de.ship.square.shop.modules.menu;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import de.ship.dbppsquare.appconfig.enums.EnumLocale;

/**
 * 
 * @author henkej
 *
 */
@RestController
public class MenuController {
	private static final Logger LOGGER = LogManager.getLogger(MenuController.class);

	@Autowired
	private MenuRepository menuRepository;

	@GetMapping(path = "/menu/{locale}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> menu(@PathVariable("locale") String locale) {
		LOGGER.debug("get translation of the menu for {}", locale);
		EnumLocale enumLocale = EnumLocale.lookupLiteral(locale);
		enumLocale = enumLocale == null ? EnumLocale.de_ : enumLocale; // fallback is de for unsupported locales
		return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON)
				.body(menuRepository.getMenu(enumLocale));
	}
}
