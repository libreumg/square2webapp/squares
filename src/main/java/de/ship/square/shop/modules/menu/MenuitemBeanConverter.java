package de.ship.square.shop.modules.menu;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;

/**
 * 
 * @author henkej
 *
 */
@Component
public class MenuitemBeanConverter implements HttpMessageConverter<MenuitemBean> {
	private static final Logger LOGGER = LogManager.getLogger(MenuitemBeanConverter.class);

	@Override
	public boolean canRead(Class<?> clazz, MediaType mediaType) {
		return clazz.equals(MenuitemBean.class) && mediaType.equals(MediaType.APPLICATION_JSON);
	}

	@Override
	public boolean canWrite(Class<?> clazz, MediaType mediaType) {
		return clazz.equals(MenuitemBean.class) && mediaType.equals(MediaType.APPLICATION_JSON);
	}

	@Override
	public List<MediaType> getSupportedMediaTypes() {
		return Arrays.asList(MediaType.APPLICATION_JSON);
	}

	@Override
	public MenuitemBean read(Class<? extends MenuitemBean> clazz, HttpInputMessage inputMessage)
			throws IOException, HttpMessageNotReadableException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		inputMessage.getBody().transferTo(out);
		return new Gson().fromJson(out.toString(), MenuitemBean.class);
	}

	@Override
	public void write(MenuitemBean bean, MediaType contentType, HttpOutputMessage outputMessage)
			throws IOException, HttpMessageNotWritableException {
		outputMessage.getHeaders().setContentType(contentType);
		String json = new Gson().toJson(bean);
		outputMessage.getBody().write(json.getBytes());
	}
}
