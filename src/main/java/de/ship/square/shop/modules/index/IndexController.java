package de.ship.square.shop.modules.index;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.websocket.server.PathParam;

import org.apache.logging.log4j.LogManager;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import de.ship.square.shop.common.SessionBean;
import de.ship.square.shop.modules.ControllerTemplate;
import de.ship.square.shop.modules.ISessionService;
import de.ship.square.shop.modules.index.impl.IndexService;
import de.ship.square.shop.modules.menu.MenuitemBeanConverter;

/**
 * 
 * @author henkej
 *
 */
@Controller
// @RolesAllowed({"read", "write"})
public class IndexController extends ControllerTemplate {
	private final static org.apache.logging.log4j.Logger LOGGER = LogManager.getLogger(IndexController.class);

	@Autowired
	private ISessionService sessionService;

	@Autowired
	private MenuitemBeanConverter menuitemBeanConverter;

	@Autowired
	private IndexService indexService;

	@Autowired
	private SessionBean sessionBean;
	
	@Value("${reference.application.menu}")
	private String menuurl;

	@GetMapping("/")
	public String homePage(Model model, HttpServletRequest request) {
		setupSession(model, request, sessionBean, sessionService, menuurl, menuitemBeanConverter);
		super.setupUserSession(request, indexService, sessionBean);
		return "index";
	}

	/**
	 * for the ajax calls - returns the json string for jsTree
	 * 
	 * @param uniqueName the unique name of the element for loading its children
	 * @param decendants if true, load all decendants of the children
	 * @param parents if true, load all parents of uniqueName, too
	 * @param request the http request
	 * @param model the model
	 * @return the json string of the array of found elements; an empty json array at least
	 * @throws ServletException on servlet errors
	 */
	@GetMapping("/load/{uniqueName}")
	public ResponseEntity<String> login(@PathVariable String uniqueName, @PathParam(value = "false") Boolean decendants, @PathParam(value = "false") Boolean parents, HttpServletRequest request, Model model) throws ServletException {
		setupSession(model, request, sessionBean, sessionService, menuurl, menuitemBeanConverter);
		super.setupUserSession(request, indexService, sessionBean);
		return ResponseEntity.ok(indexService.getElements(sessionBean.getUsername(), uniqueName, false, decendants, parents));
	}
	
	@GetMapping("/find/{needle}")
	public ResponseEntity<String> find(@PathVariable String needle, HttpServletRequest request, Model model) {
		setupSession(model, request, sessionBean, sessionService, menuurl, menuitemBeanConverter);
		return ResponseEntity.ok(indexService.find(needle));
	}

	/**
	 * for the ajax calls - return the json string of details
	 * 
	 * @param uniqueName the unique name of the element
	 * @param request the http request
	 * @param model the model
	 * @return the json string of the element; an empty object at least
	 * @throws ServletException on servlet errors
	 */
	@GetMapping("/details/{uniqueName}")
	public ResponseEntity<String> details(@PathVariable String uniqueName, HttpServletRequest request, Model model) throws ServletException {
		setupSession(model, request, sessionBean, sessionService, menuurl, menuitemBeanConverter);
		super.setupUserSession(request, indexService, sessionBean);
		return ResponseEntity.ok(indexService.getDetails(sessionBean.getUsername(), uniqueName).toJson());
	}

	@GetMapping("/login")
	public String login(HttpServletRequest request, Model model) throws ServletException {
		setupSession(model, request, sessionBean, sessionService, menuurl, menuitemBeanConverter);
		super.setupUserSession(request, indexService, sessionBean);
		model.addAttribute("elements", indexService.getElements(sessionBean.getUsername(), null, true, false, false));
		return "welcome";
	}

	@GetMapping("/logout")
	public String logout(HttpServletRequest request) throws ServletException {
		request.logout();
		return "redirect:/";
	}
	
	@RequestMapping("/logging")
	public String changeLogLevel(HttpServletRequest request, Model model, @RequestParam String level) {
		Logger logger = (Logger) LoggerFactory.getILoggerFactory().getLogger("root");
		if (logger != null) {
			logger.setLevel(Level.toLevel(level));
			LOGGER.info(String.format("Changed root logger to level %s", level));
			setupSession(model, request, sessionBean, sessionService, menuurl, menuitemBeanConverter);
		return "logging";
		} else {
			LOGGER.error(String.format("root logger not found, cannot set it to level %s", level));
			return "error";
		}
	}
}
