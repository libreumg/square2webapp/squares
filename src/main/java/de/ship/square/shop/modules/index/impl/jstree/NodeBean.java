package de.ship.square.shop.modules.index.impl.jstree;

import java.io.Serializable;

/**
 * 
 * @author henkej
 *
 */
public class NodeBean implements Serializable {
	private static final long serialVersionUID = 2L;

	private String id;
	private String parent;
	private String text;
	private String icon;
	
	public NodeBean(String id, String parent, String text, String icon) {
		super();
		this.id = id;
		this.parent = parent == null ? "#" : parent;
		this.text = text;
		this.icon = icon;
	}

	public String toString() {
		StringBuilder buf = new StringBuilder("{");
		buf.append("id: ").append(id);
		buf.append(",text: ").append(text);
		buf.append(",parent: ").append(parent);
		buf.append(",icon: ").append(icon);
		buf.append("}");
		return buf.toString();
	}
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the parent
	 */
	public String getParent() {
		return parent;
	}

	/**
	 * @param parent the parent to set
	 */
	public void setParent(String parent) {
		this.parent = parent;
	}

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * @return the icon
	 */
	public String getIcon() {
		return icon;
	}

	/**
	 * @param icon the icon to set
	 */
	public void setIcon(String icon) {
		this.icon = icon;
	}
}
