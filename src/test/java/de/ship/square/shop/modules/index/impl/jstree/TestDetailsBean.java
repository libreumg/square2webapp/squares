package de.ship.square.shop.modules.index.impl.jstree;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;

/**
 * 
 * @author henkej
 *
 */
public class TestDetailsBean {

	/**
	 * test the toJson functionality
	 */
	@Test
	public void testToJson() {
		DetailsBean bean = DetailsBean.instance("1");
		bean.set("one", 1);
		bean.set("two", new Integer[] { 1, 2 });
		assertEquals("{}", DetailsBean.instance("1").toJson());
		assertEquals("{\"one\":1,\"two\":[1,2]}", bean.toJson());
		Map<String, Integer> map = new HashMap<>();
		map.put("eins", 1);
		map.put("zwei", 2);
		map.put("drei", 3);
		bean.set("three", map);
		assertEquals("{\"one\":1,\"two\":[1,2],\"three\":{\"eins\":1,\"zwei\":2,\"drei\":3}}", bean.toJson());
	}
}
