#!/bin/bash

mkdir -p debian/squares/var/lib/squares

chmod 755 -R debian/squares/DEBIAN
chmod 644 debian/squares/DEBIAN/control

./gradlew clean build

G=$(grep "^version =" build.gradle | sed -e "s/version = //g" | sed -e "s/'//g")

echo "found version $G"

rm -f debian/squares/var/lib/squares/*.jar
cp -v build/libs/SQuaReS-$G.jar debian/squares/var/lib/squares

sed -i debian/squares/DEBIAN/control -e "s/VERSION/$G/g"
sed -i debian/squares/usr/bin/squares -e "s/VERSION/$G/g"

V=$(grep "Version" debian/squares/DEBIAN/control | sed -e "s/Version: //g")


fakeroot dpkg-deb -b -Zxz debian/squares debian/squares_$V.deb
